# remote-workstation

## Pre-requisits
- Packer >= 1.9.4
- Terraform >= 1.5.5
- Ansible >= 2.13.7

- Set the necessary environment variables with API tokens for AWS or the cloud provider of your choice if you changed it.

## Create snapshot

    cd packer
    packer build remote-workstation.pkr.hcl

## Start your workstation

    cd terraform
    terraform plan -out plan.out
    terraform apply plan.out

## Start vsode server

ssh to the server you just created and execute:

    ~/start-vscode.sh

# Warning and futur work

1. Current setup does not contain any SSL certificate so traffic to vscode in the browser is not encrypted.
Improvement available soon.

2. Github workflow will be added to automate deployment and allow start/stop the server for cost optimization

