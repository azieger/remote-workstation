terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
    }
  }
  backend "s3" {
    bucket = "workstation-terraform"
    key    = "remote-workstation"
    region = "ca-central-1"
  }
}

provider "aws" {
  region = "ca-central-1"
}

provider "cloudflare" {
  api_token = var.workstation_cloudflare_token
}